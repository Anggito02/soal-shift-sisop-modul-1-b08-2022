#!/bin/bash

timestamp=`date +%Y%m%d%H%M%S`

# target path
target=~/log

# metrics
ram=` free -m | awk 'NR>1 { for(i=2;i<=NF;i++) printf $i","; }' `
disk=` du -sh ~/ | awk '{printf $2","$1;}'`

touch $target/metrics_$timestamp.log

echo $ram$disk > $target/metrics_$timestamp.log

chmod 400 ~/log/metrics_$timestamp.log

# Cron settings
# * * * * * bash ~/Documents/sisop/praktikum/soal-shift-sisop-modul-1-b08-2022/soal3/minute_log.sh