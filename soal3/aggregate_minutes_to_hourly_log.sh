#!/bin/bash

# timestamp
time_without_min=`date +%Y%m%d%H`

# target path
target=~/log

# avg type
type=("minimum" "maximum" "average")

min_val=(999999 999999 999999 999999 999999 999999 999999 999999 999999 999999)
max_val=(0 0 0 0 0 0 0 0 0 0)

avg_val=(0 0 0 0 0 0 0 0 0 0)
sum_val=(0 0 0 0 0 0 0 0 0 0)

find_counter=0

for ((mins=0;mins<60;mins++))
do
    if [ $mins -lt 10 ]
    then
        temp_mins=0$mins
    else
        temp_mins=$mins
    fi

    for ((secs=0;secs<60;secs++))
    do
        if [ $secs -lt 10 ]
        then
            temp_secs=0$secs
        else
            temp_secs=$secs
        fi

        if [[ -f "$target/metrics_$time_without_min$temp_mins$temp_secs.log" ]]
        then
            find_counter=$(($find_counter+1))

            info=($( cat $target/metrics_$time_without_min$temp_mins$temp_secs.log | awk 'BEGIN {FS=","; OFS=" "} {for(i=1; i<=NF-2;i++) printf $i" ";printf $NF'} ))
            info[-1]=` echo ${info[-1]} | awk ' { gsub(/[A-Z]/,""); print } '`

            helper_index=0
            for value in ${info[@]}
            do
                if [[ value -lt ${min_val[helper_index]} ]]
                then
                    min_val[helper_index]=$value
                fi

                if [[ value -gt ${max_val[helper_index]} ]] 
                then
                    max_val[helper_index]=$value
                fi

                sum_val[helper_index]=$((${sum_val[helper_index]}+$value))

                helper_index=$(($helper_index+1))
            done
        fi
    done
done

if [[ $find_counter -gt 0 ]]
then
    min_str="${type[0]}"
    max_str="${type[1]}"
    avg_str="${type[2]}"

    helper_index=0;
    for value in ${min_val[@]}
    do
        avg_val[helper_index]=$((${sum_val[helper_index]}/$find_counter))
        min_str=$min_str,$value
        max_str=$max_str,${max_val[helper_index]}
        avg_str=$avg_str,${avg_val[helper_index]}

        helper_index=$(($helper_index+1))
    done
    touch $target/metrics_agg_$time_without_min.log

    sudo echo $min_str >> $target/metrics_agg_$time_without_min.log
    sudo echo $max_str >> $target/metrics_agg_$time_without_min.log
    sudo echo $avg_str >> $target/metrics_agg_$time_without_min.log

    chmod 400 $target/metrics_agg_$time_without_min.log
fi

# Cron settings
# 0 * * * * bash ~/Documents/sisop/praktikum/soal-shift-sisop-modul-1-b08-2022/soal3/aggregate_minutes_to_hourly_log.sh