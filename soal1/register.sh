#!/bin/bash

# users filepath
users_filepath=~/Documents/sisop/praktikum/res/modul-1/users/

#timestamp
timestamp=`date +"%m%d%Y %T"`

# register new user
echo " === Register New User ==="

while true
do
    echo "Enter Username :"
    read username
    if grep -E -q "^($username)( |$)" $users_filepath/user.txt
    then
        echo "Username already exists!"
        echo $timestamp "REGISTER: ERROR User already exists" >> $users_filepath/log.txt
        continue
    fi
    break
done

while true
do
    echo "Enter Password :"
    read -r -s password
    if [[ $password != $username ]] &&
        [[ "$password" =~ [:alnum] ]] &&
        [[ $password =~ [a-z] ]] &&
        [[ $password =~ [A-Z] ]] &&
        [[ `echo "$password" | wc -c` -gt 8 ]]
    then
        echo $timestamp "REGISTER: INFO User $username registered successfully" >> $users_filepath/log.txt
        echo $username $password >> $users_filepath/user.txt
        echo " === Account Registered === "
        break
    else
        echo "Password Invalid! Try Again"
    fi
done