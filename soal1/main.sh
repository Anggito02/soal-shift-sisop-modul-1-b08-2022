#!/bin/bash

# users filepath
users_filepath=~/Documents/sisop/praktikum/res/modul-1/users/

#timestamp
timestamp=`date +"%m%d%Y %T"`

echo " === LOGIN PAGE === "
while true
do
    echo "Enter username:"
    read username

    if grep -E -q "^($username)( |$)" $users_filepath/user.txt
    then
        user_account=(`grep -E "^($username)( |$)" $users_filepath/user.txt`)
        break
    else
        echo "Username not registered"
    fi
done

while true
do
    echo "Enter Password:"
    read -r -s password

    if [[ ${user_account[1]} == $password ]]
    then
        echo $timestamp "LOGIN: INFO User $username logged in" >> $users_filepath/log.txt
        break
    else
        echo "Wrong password"
        echo $timestamp "LOGIN: ERROR Failed login attempt on user $username" >> $users_filepath/log.txt
    fi
done

while true
do
    echo -e "\nWelcome, $username"
    echo "Enter 'dl N' to download N images" 
    echo "Enter 'att' to check amount of logins"
    echo "Your Input :"
    read user_input N

    if [[ $user_input == "dl" ]]
    then
        # make new user folder for images if not exists
        user_folder="`date +%Y-%m-%d`_$username"

        if [ ! -d "$users_filepath/$user_folder" ] && [ ! -f "$users_filepath/$user_folder.zip" ]
        then
            mkdir $users_filepath/$user_folder
        else 
            unzip -P $password $users_filepath/$user_folder.zip -d $users_filepath
        fi

        # find last image index
        image_index=`ls $users_filepath/$user_folder | tail -1 | awk '{ gsub(/PIC_/,""); print }'`
        if [ -z "$image_index" ]
        then
            image_index=0
        fi
        image_index=$(($image_index+1))

        # download images
        for ((i=0; i<$N; i=i+1))
        do
            if [ image_index -lt 10 ]
            then
                sudo wget -O $users_filepath/$user_folder/PIC_0$image_index https://loremflickr.com/320/240
            else
                sudo wget -O $users_filepath/$user_folder/PIC_$image_index https://loremflickr.com/320/240
            fi

            image_index=$(($image_index+1))
        done

        echo "$N image(s) downloaded"
        echo "Image(s) will be saved in $users_filepath/$user_folder"
        zip -P $password $users_filepath/$user_folder.zip $users_filepath/$user_folder

        echo " ===== Session Ended ===== "
        break

    elif [[ $user_input == "att" ]]
    then
        echo `awk /.LOGIN.*$username/ $users_filepath/log.txt | wc -l` "amount of logins had been made by $username"
        echo " ===== Session Ended ===== "
        break;
    else
        echo "Wrong Input !"
    fi
done