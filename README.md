# soal-shift-sisop-modul-1-B08-2022

## Daftar Isi

- [Anggota Kelompok](#anggota-kelompok)
- [Nomor 1](#nomor-1)
    - Deskripsi Soal
    - [Kendala Nomor 1](#kendala-nomor-1)
    - [Solusi Nomor 1](#solusi-nomor-1)
    - [Source Code Nomor 1](https://gitlab.com/Anggito02/soal-shift-sisop-modul-1-b08-2022/-/tree/main/soal1)
- [Nomor 2](#nomor-2)
    - Deskripsi Soal
    - [Kendala Nomor 2](#kendala-nomor-2)
    - [Solusi Nomor 2](#solusi-nomor-2)
    - [Source Code Nomor 2](https://gitlab.com/Anggito02/soal-shift-sisop-modul-1-b08-2022/-/tree/main/soal2)
- [Nomor 3](#nomor-3)
    - Deskripsi Soal
    - [Kendala Nomor 3](#kendala-nomor-3)
    - [Solusi Nomor 3](#solusi-nomor-3)
    - [Source Code Nomor 3](https://gitlab.com/Anggito02/soal-shift-sisop-modul-1-b08-2022/-/tree/main/soal3)
    
## Anggota Kelompok

- Anggito Anju Hartawan Manalu	        5025201216
- Muhammad Yusuf Singgih Maulana	    5025201146
- Izzati Mukhammad	                    5025201075

## Nomor 1

### Deskripsi Soal

Pada soal nomor 1, kita diminta untuk membuat (1.a) sistem register pada script `register.sh` pada komputer Han dan disimpan pada file ./users/user.txt, dan membuat sistem login pada script `main.sh`.

Kemudian, (1.b) Input password pada login dan register harus tertutup/hidden dengan kriteria sebagai berikut: 1. Minimal 8 karakter, 2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil, 3. Alphanumeric, 4. Tidak boleh sama dengan username.

(1.c) Setiap percobaan login dan register harus tercatat pada `log.txt` dengan format : MM/DD/YY hh:mm:ss <b>MESSAGE</b>. Dengan isi <b>MESSAGE</b>:

1. Saat username sudah terdaftar, message lognya "REGISTER: ERROR User already exists."
2. Saat register, message lognya "REGISTER: INFO User <b>$USERNAME</b> registered successfully"
3. Saat user login, tetapi password salah, message lognya "LOGIN: ERROR Failed login attempt on user <b>$USERNAME</b>"
4. Saat user berhasil login, message lognya "LOGIN: INFO User <b>$USERNAME</b> logged in"

(1.d) Setelah login, user dapat mengetikkan 2 command, yaitu `dl N` dan `att`. 
- Command `dl N` digunakan untuk men-<i>download</i> <b>N</b> gambar dari https://loremflickr.com/320/240 dan diberi nama <b>PIC_XX</b> (XX adalah nomor urut gambar ter-<i>download</i>) yang kemudian dimasukkan ke dalam folder dengan format nama <b>YYYY-MM-DD_USERNAME</b>. Kemudian file di-zip dengan nama yang sama, tetapi harus meng-unzip terlebih dahulu jika file zip sudah ada sebelumnya.
- Command `att` digunakan untuk menghitung jumlah percobaan login oleh user saat ini.

### Kendala Nomor 1

- Pada nomor 1, ditemukan kendala untuk mencari kata yang benar-benar tepat pada suatu file text saat menggunakan `awk`. Akhirnya digunakan `grep` untuk mendapatkan kata yang benar-benar tepat.

- `grep` memiliki sifat otomatis print hasil sehingga `grep` memiliki kekurangan tidak dapat di-assign pada suatu variabel, tanpa otomatis print hasil.

- `grep -q`,`grep --quiet`,`grep --silent` dapat digunakan untuk menggunakan `grep` tanpa otomatis print hasil. Akan tetapi, hal ini membuat `grep -q` tidak dapat disimpan hasilnya pada suatu variabel.

### Solusi Nomor 1

#### 1.a

Pada soal nomor 1.a, untuk menyimpan script `register.sh` pada file tertentu, kita dapat menggunakan `>>` untuk menuliskan sebuah text pada file text tujuan, yakni ./users/user.txt. Seperti pada register.sh pada [Source Code](https://gitlab.com/Anggito02/soal-shift-sisop-modul-1-b08-2022/-/tree/main/soal1), tertulis sebagai berikut
     
    echo $username $password >> $users_filepath/user.txt

Dimisalkan ada user baru yang akan registrasi dengan menggunakan script `register.sh` dengan username: Budi dan password: qW0il9rGzp. User baru akan muncul sebagai berikut.
<br>
<img src="/img/gambar-1-a-1.png" alt="Run register.sh, cat user.txt">


Dapat dilihat bahwa, apa yang menjadi output dari `echo $username $password` akan menjadi input pada file ./user.txt.

#### 1.b

Pada soal nomor 1.b, untuk memenuhi kriteria password yang diberikan soal, kita dapat memberikan batasan untuk input password. Selain itu, untuk memberikan sifat hidden pada password, dapat digunakan `read -s` seperti gambar berikut.
<br>
<img src="/img/gambar-1-b-1.png" alt="Kriteria password">

#### 1.c

Untuk 4 message log, kita dapat menggunakan `>>` untuk menuliskan message log untuk pada ./users/log.txt. Kita dapat menyesuaikan tiap message log sesuai dengan syarat munculnya log. Sebagai contoh, untuk memenuhi syarat log 1, 3, 4. Kita dapat menggunakan akun Budi yang telah kita buat sebelumnya. Kita dapat melihat isi log sebagai berikut.
<br>
<img src="/img/gambar-1-c-1.png" alt="Isi log.txt">

#### 1.d

Untuk command pertama, yaitu `dl N` digunakan untuk men-download gambar. Kita akan menggunakan variabel <b>$image_index</b> untuk terus mendapatkan index yang tepat untuk setiap gambar yang di-download. Untuk mendapatkan index gambar, meskipun user telah pernah men-download gambar sebelumnya, dapat digunakan fungsi `awk` dan pipelining nama file gambar. Kemudian setelah index gambar telah ditemukan, barulah image di-download menggunakan `wget -O` dimana flag `-O` digunakan untuk me-rename otomatis nama gambar yang di-download sesuai keinginan, kemudian di save pada folder user sesuai ketentuan di soal. Tak lupa kita harus meng-unzip maupun zip folder, sesuai aturan pada soal. Berikut implementasinya
<br>
<img src="/img/gambar-1-d-1.png" alt="Implementasi dl N">

Untuk command kedua, yaitu `att` kita harus menunjukkan jumlah percobaan login pada user saat ini, Kita dapat menggunakan fungsi `awk` untuk mencari log login dengan user terkait, kemudian di pipe dengan `wc -l` untuk menunjukkan banyak line pada `awk` sehingga banyak percobaan login dapat diketahui. Berikut implementasinya
<br>
<img src="/img/gambar-1-d-2.png" alt="Implementasi att">

## Nomor 2

### Deskripsi Soal

   Website https://daffa.info di hack oleh seseorang dan kita diminta untuk membaca log webiste https://daffa.info ini. Tugas pertama yang diberikan (2a) adalah kita harus membuat folder bernama forensic_log_website_daffainfo_log., pada pertanyaan kedua (2b) kita diminta untuk mencari tahu berapa rata-rata request per jam yang dikirimkan oleh penyerang ke website tersebut dan memasukan hasilnya ke dalam file ratarata.txt yang diletakan didalam folder forensic_log_website_daffainfo_log. yang sudah dibuat sebelumnya. Lalu kita juga diminta (2c) untuk menampilkan ip adress serta total request yang dkirimkan oleh ip tersebut dan hasilnya akan dimasukan ke dalam file result.txt. Berikutnya kita diminta (2d) untuk menghitung berapa banyak ip yang menggunakan user-agent curl lalu memasukan hasilnya ke dalam file result.txt. Kemudian untuk perintaan terakhir (2e) untuk soal ini adalah menampilakn ip adress yang meminta akses pada jam 2 pagi dan memasukan ip ip tersebut kedalam file result.txt.

### Kendala Nomor 2

   Terdapat beberapa kendala dalam menyelesaikan persoalan ini, yang pertama adalah pada nomor 2b output yang dikeluarkan masih salah karena pada file soal2_forensic_dapos.sh yang saya buat terdapat file interval_waktu yang isinya adalah waktu pertama dan terakhir ip miminta request pada web https://daffa.info, lalu saya menghitung total jam yang ada bedasarkan selisih waktu terakhir meminta akses dan waktu pertama kali meminta akses, lalu saat ingin menampilkan hasilnya pada file ratarata.txt saya tidak tahu kenapa terdapat isi file interval_waktu.txt yang masuk kedalam file ratarata.txt.

### Solusi Nomor 2

   Permasalahan pada nomor 2 diselesaikan dengan cara langsung membagi total request yang diminta dengan 12

#### 2a. Membuat folder
Pada soal ini kita diminta untuk membuat folder forensic_log_website_daffainfo_log.

    mkdir forensic_log_website_daffainfo_log

* pada bagian ini kita membuat folder dengan menggunakan `mkdir` yang memiliki arti make directory.

#### 2b. Menghitung rata-rata request per jam
Pada persoalan ini kami menghitung rata-rata request per jam dengan membagi total seluruh request dengan 12 

    awk '
    END {printf "Rata-rata serangan adalah sebanyak %d requests per jam", (NR-1)/12}
    ' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

* pada bagian ini fungsi awk membaca file log_website_daffainfo.log, kemudian pada bagian `(NR-1)/12` ini memiliki maksud untuk menghitung rata-rata request per jamnya sedangkan untuk `NR` sendiri adalah number of records atau menunjukan baris yang ada pada file yang dimaksudkan

#### 2c. Mencari dan menghitung ip yang paling sering muncul
Pada soal ini kami mencari serta menghitung ip yang paling sering muncul dengan menggunakan fungsi awk sebagai berikut

    awk '
    BEGIN{FS=":"}
    {if(nip[$1]++ >= n) n = nip[$1]}
    END{for(ip in nip) 
            if(n == nip[ip]) printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", ip, nip[ip] 
    }' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

* pada bagian ini pertama kami membuat array dengan `$1` didalamnya yang menunjukan ip yang dimaksudkan dan variabel n yang berfungsi untuk menyimpan frekuensi yang paling sering muncul. pada bagian `if(nip[$1]++ >= n) n = nip[$1]` memiliki maksud bahwa jika setiap ip yang dijumpai sama maka array dari nip tersebut akan bertambah. Kemudian pada bagian for loop dimana variabel ip menunjukan ip yang ada pada array nip. Pada bagian terakhir yaitu `if(n == nip[ip])` memiliki maksud bahwa ketika ditemukannya nilai yang sama dengan ip yang paling sering muncul maka program akan mencetak ip yang paling sering muncul beserta frekuensi dari ip tersebut.

#### 2d. Mencari jumlah request yang menggunakna user-agent curl
Pada soal ini kami juga menggunakan dungsi awk untuk mencari terdapat berapa ip yang meminta request dengan menggunakan user-aget curl

    awk '
    BEGIN {FS=":"}
    /curl/{++nipcurl}
    END {printf "\nAda %d requests yang menggunakan curl sebagai user-agent\n", nipcurl}
    ' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

* pada bagian ini kami hanya membuat varibel nipcurl yang akan melaukan increment setiap bertemu ip yang menggunakan user-agent curl, pada bagian ini juga kami menggunakan `>>` yang memiliki arti bahwa hasil yang ditemukan tidak dicetak ulang pada file result.txt melainkan menambahkan hasil yang ditemukan pada bagian akhir file result.txt

#### 2e. Mencetak ip yang meminta request pada jam 2
Pada soal ini kami diminta untuk mencetak seluruh ip yang meminta request pada jam 2

    awk '
    BEGIN {FS=":"}
    {if($3=="02") printf "\n%s", $1}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

* pada bagian `{FS=":"}` memiliki maksud sebagai field separator yang memiliki maksud untuk memisahkan isi file dari tanda `:` untuk mendapatkan jam yang terletak pada kolom ketiga `if($3=="02")` dan mencetak seluruh ip yang meminta request pada jam tersebut pada file result.txt

Berikut merupakan screenshot dari file ratarata.txt
![ratarata.txt!](img/ratarrata.png)

Berikut merupakan screenshot dari file result.txt
![ratarata.txt!](img/result.png)

## Nomor 3

### Deskripsi Soal

Pada nomor 3, kita diminta untuk membuat monitoring resource pada OS linux. 2 resource yang harus di-monitoring adalah ram dan size directory user. Untuk mendapatkan data ram, dapat digunakan `free -m`, sedangakan untuk mendapatkan data size directory, dapat digunakan `du -sh <target_path>`.

(3.a) Kita harus memasukkan semua metrics ke dalam file log dengan nama sesuai ketentuan pada soal, kemudian file disimpan pada /home/{user}/log. Kita harus menggunakan script yang diberi nama `minute_log.sh` untuk mendapatkan data metrics.

(3.b) Script `minute_log.sh` harus dijalankan setiap menit.

(3.c) Kemudian data dari minute_log.sh akan dibuatkan file agregasi setiap jamnya dengan menggunakan script `aggregate_minutes_to_hourly_log.sh`. Pada script agregasi ini, terdapat nilai minimum, maximum, dan rata-rata dari data metrics yang didapatkan dari script `minute_log.sh`. File agregasi juga akan menuliskan outputnya pada file log dengan nama yang ditentukan oleh soal. Selain itu, sama seperti `minute_log.sh`, `aggregate_minutes_to_hourly_log.sh` juga harus disimpan pada /home/{user}/log.

(3.d) Karena data log bersifat sensitif, file log hanya boleh dibaca oleh user pemilik file.

### Kendala Nomor 3

- Kurang efektifnya penggunaan loop pada script `aggregate_minutes_to_hourly_log.sh` karena harus looping setiap menit untuk mencari file `minute_log` pada kurun jam tersebut.

### Solusi Nomor 3

- Untuk solusi dari kendala, kemungkinan dapat digunakan regex untuk mencari file, sehingga tidak perlu menggunakan loop.

#### 3.a

Seperti yang ditulis pada soal, kita dapat menggunakan `free -m` dan `du sh <target_path>` untuk mendapatkan data-data yang diperlukan untuk dimonitoring. Untuk mendapatkan format monitoring data sesuai ketentuan di soal, dapat digunakan `awk` sebagai berikut.
<br>
<img src="/img/gambar-3-a-1.png" alt="minute_log.sh">

Dan berikut isi dari file log saat dijalankan
<br>
<img src="/img/gambar-3-a-2.png" alt="isi log">

#### 3.b

Agar script `minute_log.sh` dapat berjalan setiap menit. Maka akan dibuat cronjob sebagai berikut.
    
    * * * * * bash ~/Documents/sisop/praktikum/soal-shift-sisop-modul-1-b08-2022/soal3/minute_log.sh`.

#### 3.c

Pada script `aggregate_minutes_to_hourly_log.sh` digunakan looping untuk setiap menit dan detiknya untuk mendapatkan file yang diinginkan untuk dicari agregasinya. Implementasinya sebagai berikut.
<br>
<img src="/img/gambar-3-c-1.png" alt="looping aggregate_minutes_to_hourly_log.sh">

Akan dibuat masing-masing array untuk menyimpan nilai minimum, maximum, dan rata-rata untuk mempermudah penyimpanan data agregasi. Kemudian digunakan variabel <b>$find_counter</b> untuk menghitung file minute log pada jam tertentu. Setelah itu, untuk memperoleh setiap data yang dipisahkan oleh tanda `,` dapat digunakan fungsi `awk`. Kemudian data yang telah didapatkan dimasukkan pada array agregasi yang sesuai. Implementasinya sebagai berikut.
<br>
<img src="/img/gambar-3-c-2.png" alt="Mencari nilai agregat">

Kemudian, dapat digunakan `>>` untuk meng-append semua data agregat baru pada file agregat tiap jam. Berikut isi lognya
<br>
<img src="/img/gambar-3-c-3.png" alt="log_hourly">

(Ket. Pada log hourly didapatkan value yang sama untuk setiap fungsi agregatnya karena script `minute_log.sh` hanya dijalankan sekali sebagai contoh).

Untuk menjalankan script `aggregate_minutes_to_hourly_log.sh` setiap jamnya, dapat digunakan cronjob sebagai berikut.

    0 * * * * bash ~/Documents/sisop/praktikum/soal-shift-sisop-modul-1-b08-2022/soal3/aggregate_minutes_to_hourly_log.sh

#### 3.d

Untuk memberikan akses baca hanya pada pembuat file, kita dapat menggunakan

    chmod 400 ~/log/{nama_file}.log
