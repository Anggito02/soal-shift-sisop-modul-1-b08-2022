#!/bin/bash

#a
mkdir forensic_log_website_daffainfo_log

#b
#menghitung rata rata serangan perjam dengan membagi total request dengan 12
awk '
    END {printf "Rata-rata serangan adalah sebanyak %d requests per jam", (NR-1)/12}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

#c
#menampilkan dan menghitung ip yang paling banyak meminta akses server
awk '
    BEGIN{FS=":"}
    {if(nip[$1]++ >= n) n = nip[$1]}
    END{for(ip in nip) 
            if(n == nip[ip]) printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n", ip, nip[ip] 
    }' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

#d
#menghitung banyaknya request yang menggunakan user-agent curl
awk '
    BEGIN {FS=":"}
    /curl/{++nipcurl}
    END {printf "\nAda %d requests yang menggunakan curl sebagai user-agent\n", nipcurl}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

#e
#mencetak ip yang mengakses web https://daffa.info pada jam 2 pagi pada tanggal 22
awk '
    BEGIN {FS=":"}
    {if($3=="02") printf "\n%s", $1}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt